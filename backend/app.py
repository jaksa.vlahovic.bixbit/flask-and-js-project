from flask import Flask, jsonify
from flask_restful import Api, Resource, reqparse
from http import HTTPStatus
from db import Database
from config_env import config
from flask_cors import CORS

app = Flask(__name__)
api = Api(app)
CORS(app)
db = Database(config)
parser = reqparse.RequestParser()

def get_response_msg(data, status_code):
    message = {
        'status': status_code,
        'data': data if data else 'No data'
    }
    response_msg = jsonify(message)
    response_msg.status_code = status_code
    return response_msg

class EmployeeList(Resource):
    def get(self):
        query = """
        SELECT e1.employeeNumber, e1.firstName, e1.lastName, e1.email, e1.officeCode, e1.extension, e1.reportsTo, e2.firstName AS reportsToFirstName, e2.lastName AS reportsToLastName, e1.jobTitle
        FROM employees e1
        JOIN employees e2
        ON e1.reportsTo = e2.employeeNumber;
        """
        data = db.run_query(query)
        response = get_response_msg(data, HTTPStatus.OK)
        db.close_connection()
        return response
    
    def post(self):
        parser.add_argument('employeeNumber', type=int, required=True)
        parser.add_argument('firstName', type=str, required=True)
        parser.add_argument('lastName', type=str, required=True)
        parser.add_argument('extension', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('officeCode', type=int, required=True)
        parser.add_argument('reportsTo', type=int, required=False)
        parser.add_argument('jobTitle', type=str, required=True)
        args = parser.parse_args()
        
        columns = list(args.keys())
        values = values = [f"'{value}'" if isinstance(value, str) else str(value) for value in args.values()]
        query = "INSERT INTO employees ({}) VALUES ({})".format(', '.join(columns), ', '.join(values))
        print(query)
        data = db.run_query(query)
        response = get_response_msg(data, HTTPStatus.CREATED)
        db.close_connection
        
        return response
    

class Employee(Resource):
    def put(self, employeeNumber):
        parser.add_argument('employeeNumber', type=int, required=True)
        parser.add_argument('firstName', type=str, required=True)
        parser.add_argument('lastName', type=str, required=True)
        parser.add_argument('extension', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        parser.add_argument('officeCode', type=int, required=True)
        parser.add_argument('reportsTo', type=int, required=False)
        parser.add_argument('jobTitle', type=str, required=True)
        args = parser.parse_args()
        
        columns = list(args.keys())
        values = values = [f"'{value}'" if isinstance(value, str) else str(value) for value in args.values()]
        query = "UPDATE employees SET " + ", ".join([f"{col} = {val}" for (col, val) in zip(columns, values)]) + f" WHERE employeeNumber={employeeNumber}"
        data = db.run_query(query)
        response = get_response_msg(data, HTTPStatus.OK)
        db.close_connection
        
        return response
    
    def delete(self, employeeNumber):
        query = f"DELETE FROM employees WHERE employeeNumber = {employeeNumber};"
        data = db.run_query(query)
        response = get_response_msg(data, HTTPStatus.OK)
        db.close_connection()
        return response
        

class Office(Resource):
    def get(self, office_code):
        query = f"SELECT * FROM offices WHERE officeCode = {office_code};"
        data = db.run_query(query)
        response = get_response_msg(data, HTTPStatus.OK)
        db.close_connection()
        return response

api.add_resource(EmployeeList, '/employees')
api.add_resource(Employee, '/employees/<int:employeeNumber>')
api.add_resource(Office, '/office/<int:office_code>')

if __name__ == '__main__':
    app.run(debug=True)