config = {
    'DB_USER': '',
    'DB_PASSWD': '',
    'DB_HOST': '127.0.0.1',
    'DB_PORT': 3306,
    'DB_NAME': 'classicmodels',
    'CONNECT_TIMEOUT': 5
}